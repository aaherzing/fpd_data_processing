Welcome to fpd_data_processing's documentation!
===============================================

News
----
**2018-2-18: fpd_data_processing 0.2.1 released!**

This release includes a major improvement for lazy signal processing with center of mass and radial integration.
It also includes a new method for shifting diffraction patterns in the PixelatedSTEM class.


About fpd_data_processing
-------------------------

Library for processing data acquired on a fast pixelated electron detector, acquired using scanning transmission electron microscopy.

Install instructions: :ref:`install`.

fpd_data_processing is available under the GNU GPL v3 license, and the source code is found in the `GitLab repository <https://gitlab.com/fast_pixelated_detectors/fpd_data_processing/tree/master/>`_.

.. image:: images/frontpage/stem_diffraction.jpg
    :scale: 49 %

.. image:: images/frontpage/dpc_dummy_data.jpg
    :scale: 49 %


.. toctree::
   install
   loading_data
   using_pixelated_stem_class
   analysing_holz_datasets
   analysing_dpc_datasets
   generate_test_data
   misc_functions
   related_projects
   api
   :maxdepth: 2
   :caption: Contents:

Contributions from Magnus Nord funded by EPSRC via the project "Fast Pixel Detectors: a paradigm shift in STEM imaging" (Grant reference EP/M009963/1).

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
