.. _api:

=================
API documentation
=================

Classes
-------

PixelatedSTEM
=============
.. autoclass:: fpd_data_processing.pixelated_stem_class.PixelatedSTEM
    :members:
    :undoc-members:

DPCBaseSignal
=============
.. autoclass:: fpd_data_processing.pixelated_stem_class.DPCBaseSignal
    :members:
    :undoc-members:

DPCSignal1D
===========
.. autoclass:: fpd_data_processing.pixelated_stem_class.DPCSignal1D
    :members:
    :undoc-members:

DPCSignal2D
===========
.. autoclass:: fpd_data_processing.pixelated_stem_class.DPCSignal2D
    :members:
    :undoc-members:

Modules
-------

IO tools
========
.. automodule:: fpd_data_processing.io_tools
    :members:
    :undoc-members:

Radial
======
.. automodule:: fpd_data_processing.radial
    :members:
    :undoc-members:

Dummy data
==========
.. automodule:: fpd_data_processing.dummy_data
    :members:
    :undoc-members:

Make diffraction test data
==========================
.. automodule:: fpd_data_processing.make_diffraction_test_data
    :members:
    :undoc-members:

