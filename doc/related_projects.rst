.. _related_projects:

================
Related projects
================

- `fpd_live_imaging <https://fast_pixelated_detectors.gitlab.io/fpd_live_imaging/>`_: for doing live processing and imaging of data acquired on a fast pixelated detector using a scanning transmission electron microscope.
- `merlin_interface <https://fast_pixelated_detectors.gitlab.io/merlin_interface/>`_: Python library for interfacing with a Medipix3 detector through the Merlin software
